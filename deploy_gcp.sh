#!/usr/bin/env bash

if [[ -z "${DEFAULT_GOOGLE_PROJECT}" ]]; then
  echo "You must set the DEFAULT_GOOGLE_PROJECT variable to your project name"
  exit 1
fi

HOST="conferencetitle.zvan.net"
DEPLOYMENT="conference-title"
REPO_NAME="us-docker.pkg.dev/${DEFAULT_GOOGLE_PROJECT}/us.gcr.io"

IMAGE_NAME="${REPO_NAME}/${DEPLOYMENT}"

DATE=$(date +%Y%m%d%H%M)
echo "Tagging with ${DATE}"

echo building
docker build . -t "${HOST}:latest" -t "${HOST}:${DATE}"
echo tagging
docker tag "${HOST}:latest" "${IMAGE_NAME}:latest"
docker tag "${HOST}:latest" "${IMAGE_NAME}:${DATE}"
echo pushing
docker push "${IMAGE_NAME}:latest"
docker push "${IMAGE_NAME}:${DATE}"

echo deploying
kubectl apply -f k8s/deployment.yaml
kubectl apply -f k8s/service.yaml
kubectl set image deployment/conference-title conference-title="${IMAGE_NAME}:${DATE}"
