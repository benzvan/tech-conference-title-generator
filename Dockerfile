# Test docker container
# might run conferencetitle.zvan.net
FROM php:8.1.19-apache
MAINTAINER ben@zvan.net
RUN apt-get update
RUN ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load

RUN sed -i -e 's#combined#"{\\\"remoteIP\\\":\\\"%h\\\",\\\"user\\\":\\\"%u\\\",\\\"timestamp\\\":\\\"%t\\\",\\\"protocol\\\":\\\"%H\\\",\\\"method\\\":\\\"%m\\\",\\\"path\\\":\\\"%U\\\",\\\"q\\\":\\\"%q\\\",\\\"status\\\":\\\"%>s\\\",\\\"bytes\\\":\\\"%b\\\"}"#' \
         /etc/apache2/sites-enabled/000-default.conf

COPY  src/main/webapp/ /var/www/html/ 
EXPOSE 80
